const fs=require('fs');
const http=require('http');
const url=require('url');
const express=require('express');
const app=express();
const server = http.createServer((req,res) => {
	app.get('/',function(req,res) {
  		res.sendFile('./template/index.html');
	});
});
server.listen(8000,'127.0.0.1',()=>{
	console.log('listening to requests on port 8000');
});