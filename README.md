# Malware.ai
Its basically classification and detection of malware using various Machine Learning and Deep Learning algorithms. Future expansion of this project is to make a common platform of malware detection and classification using Flask so that people can check whether the softwares or the URL they visit is malicious or not.

# Malviz-Web

[Malviz-Web](https://gitlab.com/shivamkataria2000/malviz.ai)
 is the web application for this tool.


## Demo

[malviz.ai](http://malviz.herokuapp.com/)


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
