from flask import Flask,render_template
app = Flask(__name__)

@app.route('/')
def index_page():
	return render_template('index.html')

@app.route('/upload')
def file_upload():
	return render_template('upload.html')


@app.route('/upload-file', methods=['POST'])
def file():
	return render_template('upload.html')

if __name__=="__main__":
	app.run(debug=True)